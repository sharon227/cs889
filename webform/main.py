from flask import Flask, render_template, request, redirect
import json
import random


app = Flask(__name__, template_folder='.')


    
class RestaurantReview(object):
    def __init__(self, review_id="", review_text="",
        list_of_phrases=[]):

        self.review_id = review_id
        self.review_text = review_text
        self.list_of_phrases = list_of_phrases
        
        
    def put_spans_in(self):
        
        ret_str = ""
        curr_char = 0
       
        for phrase_tuple in self.list_of_phrases:
            ret_str += self.review_text[curr_char:int(phrase_tuple[0])] + "<span data-start='%s' data-sentiment='%s' data-end='%s' class='%s'>" % (int(phrase_tuple[0]), phrase_tuple[2], int(phrase_tuple[1]), phrase_tuple[2]) +  self.review_text[int(phrase_tuple[0]):int(phrase_tuple[1])]  + "</span>"
            curr_char = int(phrase_tuple[1])
            
        ret_str += self.review_text[curr_char:]
        return ret_str

def read_user_list():
    returned_dict = {}
    with open('id_list.txt', 'r') as f:
        info_list = f.readlines()
        
        for line in info_list:
            user_id, list_of_review_ids = line.split()
            list_of_review_ids = list_of_review_ids.split(",")
            returned_dict[user_id] = list_of_review_ids
            
    return returned_dict
    


# GLOBAL CONSTANTS

completed_text = "COMPLETED REQUIRED REVIEWS.  CLICK SAVE AGAIN TO RESTART THE PROCESS"

OUTPUT_FILE = "user_study_results.txt"
USER_REVIEW_DICT = read_user_list()
REVIEWS = {}

with open("reviews_with_sentiment_clean.json", "r") as f:
    
    reviews = json.loads(f.read())
    for item in reviews:
        # Note: list of phrases should not be empty.  But we will
        # do this later
        
        #go through sentiment_info and pickout all the dictionaries and
        #make them into tuples
        sentiment_info_list = item['sentiment_info']
        #print sentiment_info_list
        
        list_of_phrases = []
        
        for sentiment_dict in sentiment_info_list:
            
            
            try:
                label = sentiment_dict['sentiment']['label']
                list_of_phrases.append((sentiment_dict['begin_index'],
                    sentiment_dict['end_index'], label, sentiment_dict['sentiment']['probability'][label]))
            except KeyError, e:
                print "This dictionary is missing a begin index %s" % sentiment_dict
        
        r = RestaurantReview(item['review_id'], item['text'], list_of_phrases)

        REVIEWS[item['review_id']] = r


@app.route('/', methods=['GET', 'POST'])
def page_list():
    ids = sorted(map(int, USER_REVIEW_DICT.keys()))
    return ''.join(['<li><a href="/%s">%s</a></li>' % (x, x) for x in ids])

@app.route('/<userID>')
def user_main(userID):
    return redirect('/%s/%s' % (userID, USER_REVIEW_DICT[userID][0]))

@app.route('/<userID>/<reviewID>', methods=['GET', 'POST'])
def main(userID, reviewID):
    global completed_text
    global OUTPUT_FILE

    review_id = reviewID
    
    try:
        current_index = USER_REVIEW_DICT[userID].index(reviewID)
    except ValueError:
        current_index = 0
    
    allow_change = False
    instructions = "Select the most appropriate star rating"
    
    if review_id == 'done':
        review_text = completed_text
    elif current_index >= 10:
        if current_index >= 20:
            allow_change = True
            instructions = "Give us the most appropriate star rating, <b> and change the sentiment of the highlighted phrases if you feel that it is appropriate to do so </b>.  Red indicates negative sentiment, green indicates positive sentiment, and yellow indicates neutral sentiment"
        else:
            instructions = "Give us the most appropriate star rating. Red indicates negative sentiment, green indicates positive sentiment, and yellow indicates neutral sentiment"                
        
        review_text = REVIEWS[review_id].put_spans_in()
    else:
        review_text = REVIEWS[review_id].review_text
        
    
    if request.method == 'POST':
        prev_index_review = USER_REVIEW_DICT[userID].index(request.form['review_id'])
        current_index = prev_index_review + 1
        
        print "prev_index_review: %s"  % (prev_index_review)

        if current_index < 10:
            review_id = USER_REVIEW_DICT[userID][prev_index_review + 1]
            
        elif len(USER_REVIEW_DICT[userID]) > prev_index_review + 1:
            review_id = USER_REVIEW_DICT[userID][prev_index_review + 1]

        else:
            review_id = 'done'

        new_dict = {}
        new_dict['user_id'] = userID
        new_dict['review_id'] = request.form['review_id']
        new_dict['stars'] = request.form['stars']
        new_dict['sentiment'] = json.loads(request.form['hidden_sentiment'])
        
        returned_information = (json.dumps(request.form))
        with open(OUTPUT_FILE, "a") as f:
            f.write("%s\n" % (json.dumps(new_dict)))
        return redirect('/%s/%s' % (userID, review_id))
    return render_template('main.html', review_text=review_text, review_id=review_id, allow_change=allow_change, instructions=instructions)

if __name__ == '__main__':
    app.run(debug=True)
    
