# Generate the random order that reviews are presented in.

import json, random, sys
from collections import defaultdict

reviews = json.loads(sys.stdin.read())

review_ids = [review['review_id'] for review in reviews]

for i in range(20):
    random.shuffle(review_ids)
    print("%s %s" % (i, ','.join(review_ids)))
