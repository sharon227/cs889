# Select the reviews from the Yelp Dataset

import json, random, sys
from collections import defaultdict

reviews = {}
reviews_by_stars = defaultdict(list)
reviews_per_star = 6
char_range = [94, 3125]
bad_word = 'star'

categories = set(['Restaurants', 'Food', 'Bars'])
business_ids = []
with open('yelp_dataset/yelp_academic_dataset_business.json') as f:
    for line in f:
        business = json.loads(line)
        if len(categories & set(business['categories'])) == 0:
            continue
        business_ids.append(business['business_id'])
business_ids = set(business_ids)

for line in sys.stdin:
    review = json.loads(line)
    if len(review['text']) < char_range[0] or len(review['text']) > char_range[1]:
        continue
    if review['text'].find(bad_word) != -1:
        continue
    if review['business_id'] not in business_ids:
        continue
    reviews_by_stars[review['stars']].append(review['review_id'])
    reviews[review['review_id']] = {key: review[key] for key in ['review_id', 'stars', 'text']}

filtered_ids = []
for stars, review_ids in reviews_by_stars.items():
    random.shuffle(review_ids)
    filtered_ids.extend(review_ids[:reviews_per_star])

filtered_reviews = []
for review_id in filtered_ids:
    filtered_reviews.append(reviews[review_id])

print(json.dumps(filtered_reviews))
