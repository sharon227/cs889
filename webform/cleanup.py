import json, re

with open('reviews_with_sentiment.json') as f:
    data = json.loads(f.read())

for i in range(len(data)):
    for key in data[i]:
        new_key = re.sub('\s+', '', key)
        data[i][new_key] = data[i].pop(key)
    for j in range(len(data[i]['sentiment_info'])):
        for key in data[i]['sentiment_info'][j]:
            new_key = re.sub('\s+', '', key)
            data[i]['sentiment_info'][j][new_key] = data[i]['sentiment_info'][j].pop(key)
        for key in data[i]['sentiment_info'][j]['sentiment']:
            new_key = re.sub('\s+', '', key)
            data[i]['sentiment_info'][j]['sentiment'][new_key] = data[i]['sentiment_info'][j]['sentiment'].pop(key)
        for key in data[i]['sentiment_info'][j]['sentiment']['probability']:
            new_key = re.sub('\s+', '', key)
            data[i]['sentiment_info'][j]['sentiment']['probability'][new_key] = data[i]['sentiment_info'][j]['sentiment']['probability'].pop(key)
        data[i]['sentiment_info'][j]['sentiment']['label'] = re.sub('\s+', '', data[i]['sentiment_info'][j]['sentiment']['label'])

print(json.dumps(data))
